/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package appfuzzy;

import java.util.Scanner;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

/**
 *
 * @author User
 */
public class AppFuzzy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //String nomeArquivo= "E:\\XI - UNAM - I\\inteligencia artificial\\padraoFCL.fcl";
        /*String nomeArquivo= "C:\\Users\\User\\Documents\\NetBeansProjects\\CasoFuzzy\\padraoFCL.fcl";
        FIS funcao= FIS.load(nomeArquivo);
        Scanner entrada =new Scanner (System.in);
        System.out.println("Digite o valor de capacitacao: ");
        double capacitacao=entrada.nextDouble();
        System.out.println("Digite o valor de experiencia: ");
        double experiencia=entrada.nextDouble();
        
        funcao.setVariable("capacitacao", capacitacao);
        funcao.setVariable("experiencia", experiencia);
        funcao.evaluate();
        
        Variable gratificacao = funcao.getVariable("gratificacao");
        JFuzzyChart.get().chart(gratificacao,gratificacao.getDefuzzifier(),true);
        JFuzzyChart.get().chart(funcao);*/
        
        String nomeArquivo= "E:\\XI - UNAM - I\\inteligencia artificial\\irrigacaoFCL.fcl";
        FIS funcao= FIS.load(nomeArquivo);
        Scanner entrada =new Scanner (System.in);
        System.out.println("Digite o valor de temperatura: ");
        double temperatura=entrada.nextDouble();
        System.out.println("Digite o valor de umidade: ");
        double umidade=entrada.nextDouble();
        
        funcao.setVariable("temperatura", temperatura);
        funcao.setVariable("umidade", umidade);
        funcao.evaluate();
        
        Variable irrigacao = funcao.getVariable("irrigacao");
        JFuzzyChart.get().chart(irrigacao,irrigacao.getDefuzzifier(),true);
        JFuzzyChart.get().chart(funcao);
        
    }
    
}
